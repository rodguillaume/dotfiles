# TODO
# add ppa dukto + GPG key
# add webupb8 to install *forgot, TODO check launchpad webupb8 packages*

sudo apt update
sudo apt install -y vim git vlc snapd chromium-browser rofi

snap install spotify mailspring
snap install --classic atom

apm install vim-mode-plus platformio-ide-terminal minimap atom-beautify file-icons project-manager atom-ternjs docblockr atom-doxit es6-javascript language-babel

sudo apt upgrade -y
sudo apt dist-upgrade -y

sudo apt autoremove -y
sudo apt autoclean -y
